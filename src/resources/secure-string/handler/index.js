const AWS = require('aws-sdk');
const crypto = require('crypto');

async function onDelete(props) {
	const ssm = new AWS.SSM();

	return ssm.deleteParameter({
		Name: props.Name
	}).promise();
}

async function onUpdate(props, oldProps) {
	const ssm = new AWS.SSM();

	if (props.Name !== oldProps.Name) {
		const oldParameter = await ssm.getParameter({
			Name: oldProps.Name,
			WithDecryption: true
		}).promise();

		const value = oldParameter.Parameter.Value;
		await onDelete(oldProps);
		await onCreate({
			...props,
			InitialValue: value
		});
	}

	return createValueResponse(props);
}

async function onCreate(props) {
	const ssm = new AWS.SSM();

	const value = props.InitialValue ? props.InitialValue : generateRandomString(props.Generate?.Length);

	await ssm.putParameter({
		Name: props.Name,
		Value: value,
		Type: 'SecureString',
	}).promise();

	return createValueResponse(props);
}

async function createValueResponse(props) {
	const ssm = new AWS.SSM();

	const param = await ssm.getParameter({
		Name: props.Name,
	}).promise();

	return {
		Data: {
			...param.Parameter,
			NoEcho: true
		}
	};
}

function generateRandomString(length = 20) {
	return crypto.randomBytes(parseInt(length)).toString('base64').slice(0, length);
}


exports.handler = (event) => {
	console.log(event);
	const { RequestType, ResourceProperties, OldResourceProperties } = event;

	switch (RequestType) {
		case 'Create':
			return onCreate(ResourceProperties);
		case 'Update':
			return onUpdate(ResourceProperties, OldResourceProperties);
		case 'Delete':
			return onDelete(ResourceProperties);
	}
};
