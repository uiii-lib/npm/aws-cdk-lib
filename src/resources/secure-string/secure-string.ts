import path from "path";
import { CustomResource, Stack } from "aws-cdk-lib";
import { Effect, PolicyStatement } from "aws-cdk-lib/aws-iam";
import { Code, Function, Runtime } from "aws-cdk-lib/aws-lambda";
import { Provider } from "aws-cdk-lib/custom-resources";
import { Construct } from "constructs";

export interface SecureStringProps {
	Name: string;
	InitialValue?: string;
	Generate?: {
		Length?: number;
	}
}

export class SecureString extends CustomResource {
	constructor(scope: Construct, id: string, props: SecureStringProps) {
		super(scope, id, {
			serviceToken: SecureStringProvider.getServiceToken(scope),
			resourceType: 'Custom::SecureString',
			properties: {
				...props
			}
		});
	}

	get attrArn() {
		return this.getAtt('ARN').toString();
	}

	get attrValue() {
		return this.getAtt('Value').toString();
	}
}

export class SecureStringProvider extends Construct {
	private readonly provider: Provider;

	constructor(scope: Construct, id: string) {
		super(scope, id);

		const onEvent = new Function(this, 'Handler', {
			handler: 'index.handler',
			code: Code.fromAsset(path.join(__dirname, '..', '..', '..', '..', 'src', 'resources', 'secure-string', 'handler')),
			runtime: Runtime.NODEJS_16_X,
			initialPolicy: [
				new PolicyStatement({
					actions: ['ssm:*'],
					effect: Effect.ALLOW,
					resources: [
						'*'
					]
				})
			]
		});

		this.provider = new Provider(this, 'Provider', {
			onEventHandler: onEvent,

		});
	}

	public static getServiceToken(scope: Construct) {
		const stack = Stack.of(scope);
		const id = 'SecureStringProvider';

		let instance = stack.node.tryFindChild(id) as SecureStringProvider;

		if (!instance) {
			instance = new SecureStringProvider(stack, id);
		}

		return instance.provider.serviceToken;
	}
}
