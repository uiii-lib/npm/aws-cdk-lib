import { CfnParameter, CfnParameterProps, Stack } from "aws-cdk-lib";

import { capitalize } from "./helpers/capitalize";

export interface Parameter<T> {
	cfnParameter?: CfnParameter;
	value: T;
}

export type ParameterGroup = {
	Label: {
		default: string;
	},
	Parameters: string[];
}

export abstract class Preset<T = never> {
	constructor(stack: Stack) {
		// add "Other" parameter group to the end
		this.addParameterGroup(stack, "Other", (parameters) => parameters);
	}

	protected buildParameters<PK extends keyof T>(stack: Stack, parametersProps: Record<PK, CfnParameterProps>, parametersValues: Partial<T> = {}) {
		const parameters: Partial<{[K in keyof T]: Parameter<T[K]>}> = {};
		const values: Partial<T> = {};

		for (const key of Object.keys(parametersProps)) {
			if (key in parametersValues) {
				const value = parametersValues[key as keyof T]!;

				parameters[key as keyof T] = {
					value
				};

				values[key as keyof T] = value;
			} else {
				const parameter = new CfnParameter(stack, capitalize(key), parametersProps[key as PK]);
				const value = this.getParameterValue(parameter) as any;

				parameters[key as PK] = {
					cfnParameter: parameter,
					value
				};

				values[key as PK] = value;
			}
		}

		return {
			...parameters as typeof parameters & Required<{[K in PK]: Parameter<T[K]>}>,
			values: values as Partial<T> & Required<{[K in PK]: T[K]}>
		};
	}

	protected addParameterGroup(
		stack: Stack,
		groupLabel: string,
		parametersSetter: string[] | ((existingParameters: string[]) => string[]),
		atIndex?: (existingGroupLabels: string[]) => number|undefined
	) {
		stack.templateOptions.metadata ??= {};
		stack.templateOptions.metadata['AWS::CloudFormation::Interface'] ??= {};
		stack.templateOptions.metadata['AWS::CloudFormation::Interface'].ParameterGroups ??= [];

		const groups = stack.templateOptions.metadata['AWS::CloudFormation::Interface'].ParameterGroups as ParameterGroup[];
		const groupLabels = groups.map(it => it.Label.default);

		let group = groups.find(it => it.Label.default === groupLabel);
		let index = atIndex?.(groupLabels);

		if (!group) {
			group = {
				Label: {
					default: groupLabel
				},
				Parameters: []
			};

			if (index === undefined) {
				groups.push(group);
			} else {
				groups.splice(index, 0, group);
			}
		} else {
			if (index !== undefined && groups.indexOf(group) !== index) {
				groups.splice(groups.indexOf(group), 1); // remove from incorrect place
				groups.splice(index, 0, group); // insert into correct place
			}
		}

		if (Array.isArray(parametersSetter)) {
			if (group.Parameters.length > 0) {
				console.warn(`WARNING: There are already parameters in the '${groupLabel}' group and will be overwritten. Use a function setter instead.`);
			}

			group.Parameters = parametersSetter.map(it => capitalize(it));
		} else {
			group.Parameters = parametersSetter(group.Parameters).map(it => capitalize(it));
		}

		if (groupLabel !== "Other") {
			// if any groups are used, add the "Other" group for uncategoried parameter and keep it at the end
			this.addParameterGroup(stack, "Other", (parameters) => parameters, (groups) => groups.length);
		}
	}

	protected getParameterValue(parameter: CfnParameter) {
		if (parameter.type === "Number") {
			return parameter.valueAsNumber;
		} else if (
			parameter.type === "CommaDelimitedList"
			|| parameter.type.match(/List<[^<>]+>/)
		) {
			return parameter.valueAsList;
		} else {
			return parameter.valueAsString;
		}
	}
}
