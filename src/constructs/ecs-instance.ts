import fs from "fs";
import { aws_ec2, aws_iam, CfnCondition, Fn, Stack } from "aws-cdk-lib";
import { CfnCluster } from "aws-cdk-lib/aws-ecs";
import { Construct } from "constructs";
import path from "path";
import { CfnRole } from "aws-cdk-lib/aws-iam";

export interface Ec2InstanceProps {
	cluster: CfnCluster;
	instanceType: string;
	instanceImageId: string;
	swapSize: number;
	ipAllocationId: string;
	dataVolumeId: string;
	subnetId: string;
	securityGroupId: string;
	sshAuthorizedKeysId: string;
	permissions?: {
		policyStatements?: aws_iam.PolicyStatement[];
		managedPolicyNames?: string[];
	},
	createInstance?: 'true'|'false',
	useSpotInstance?: 'true'|'false',
}

export class EcsInstance extends Construct {
	constructor(scope: Construct, id: string, props: Ec2InstanceProps) {
		super(scope, id);

		const {
			cluster,
			instanceType,
			instanceImageId,
			swapSize,
			ipAllocationId,
			dataVolumeId,
			subnetId,
			securityGroupId,
			sshAuthorizedKeysId,
			createInstance = 'true',
			useSpotInstance = 'false'
		} = props;

		/** CONDITIONS **/

		const useOnDemandInstanceCondition = new CfnCondition(this, 'UseOnDemandInstanceCondition', {
			expression: Fn.conditionAnd(
				Fn.conditionEquals(createInstance, 'true'),
				Fn.conditionEquals(useSpotInstance, 'false')
			)
		});

		const useSpotInstanceCondition = new CfnCondition(this, 'UseSpotInstanceCondition', {
			expression: Fn.conditionAnd(
				Fn.conditionEquals(createInstance, 'true'),
				Fn.conditionEquals(useSpotInstance, 'true')
			)
		});

		/** RESOURCES **/

		const instanceRole = this.createInstanceRole(props);
		const spotFleetRole = this.createSpotFleetRole(props);

		spotFleetRole.cfnOptions.condition = useSpotInstanceCondition;

		const instanceProfile = new aws_iam.CfnInstanceProfile(this, 'InstanceProfile', {
			roles: [
				instanceRole.ref
			]
		});

		const launchTemplate = new aws_ec2.CfnLaunchTemplate(this, 'LaunchTemplate', {
			launchTemplateName: Stack.of(this).stackName,
			launchTemplateData: {
				imageId: instanceImageId,
				iamInstanceProfile: {
					arn: instanceProfile.attrArn
				},
				instanceType,
				securityGroupIds: [
					securityGroupId
				],
				blockDeviceMappings: [
					{
						deviceName: '/dev/xvda',
						ebs: {
							volumeSize: 8,
							volumeType: aws_ec2.EbsDeviceVolumeType.GP2.toString()
						}
					}
				],
				userData: Fn.base64(
					Fn.sub(fs.readFileSync(path.join(__dirname, '..', '..', '..', 'src', 'constructs', 'ec2-instance-user-data.sh'), 'utf-8'), {
						swapSize: swapSize.toString(),
						ipAllocationId,
						dataVolumeId,
						sshAuthorizedKeysId,
						cluster: cluster.ref
					})
				)
			},
		});

		const instance = new aws_ec2.CfnInstance(this, 'Ec2Instance', {
			launchTemplate: {
				launchTemplateId: launchTemplate.ref,
				version: launchTemplate.attrLatestVersionNumber
			},
			subnetId
		});

		instance.cfnOptions.condition = useOnDemandInstanceCondition;

		const spotFleet = new aws_ec2.CfnSpotFleet(this, 'SpotFleet', {
			spotFleetRequestConfigData: {
				allocationStrategy: 'lowestPrice',
				launchTemplateConfigs: [
					{
						launchTemplateSpecification: {
							launchTemplateId: launchTemplate.ref,
							version: launchTemplate.attrLatestVersionNumber
						},
						overrides: [
							{
								subnetId
							}
						]
					}
				],
				targetCapacity: 1,
				iamFleetRole: spotFleetRole.attrArn
			}
		});

		spotFleet.cfnOptions.condition = useSpotInstanceCondition;
	}

	protected createInstanceRole(props: Ec2InstanceProps) {
		const {permissions} = props;

		const instanceRole = new aws_iam.Role(this, 'InstanceRole', {
			assumedBy: new aws_iam.ServicePrincipal('ec2.amazonaws.com'),
		});

		instanceRole.addToPolicy(new aws_iam.PolicyStatement({
			effect: aws_iam.Effect.ALLOW,
			actions: [
				'ec2:AttachVolume',
				'ec2:AssociateAddress',
				'ec2:DescribeVolumes',
				'ssm:GetParameter'
			],
			resources: ['*']
		}));

		instanceRole.addManagedPolicy(aws_iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AmazonEC2ContainerServiceforEC2Role'));

		for (let policy of permissions?.policyStatements || []) {
			instanceRole.addToPolicy(policy);
		}

		for (let managedPolicyName of permissions?.managedPolicyNames || []) {
			instanceRole.addManagedPolicy(aws_iam.ManagedPolicy.fromAwsManagedPolicyName(managedPolicyName));
		}

		return instanceRole.node.defaultChild as CfnRole;
	}

	protected createSpotFleetRole(props: Ec2InstanceProps) {
		const spotFleetRole = new aws_iam.Role(this, 'SpotFleetRole', {
			assumedBy: new aws_iam.ServicePrincipal('spotfleet.amazonaws.com'),
		});

		spotFleetRole.addManagedPolicy(aws_iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AmazonEC2SpotFleetTaggingRole'));

		return spotFleetRole.node.defaultChild as CfnRole;
	}
}
