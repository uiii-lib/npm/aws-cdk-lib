#!/bin/bash
INSTANCE_ID=$(ec2-metadata -i | cut -d" " -f2)

# install packages
dnf check-update
dnf install -y awscli htop vim

# install EC2 Instance Connect
dnf install -y ec2-instance-connect
#sed -i '$s;$; || curl -sf https://uiii.gitlab.io/vps-authorized-keys/${sshAuthorizedKeysId};' /opt/aws/bin/eic_run_authorized_keys

# enable swap (1024GB)
dd if=/dev/zero of=/swapfile bs=128M count=$(expr ${swapSize} / 128)
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo "/swapfile swap swap defaults 0 0" >> /etc/fstab

# attach elastic IP address
aws ec2 associate-address --allocation-id ${ipAllocationId} --instance-id ${!INSTANCE_ID} --region ${AWS::Region}

# attach data volume
aws ec2 wait volume-available --volume-ids ${dataVolumeId} --region ${AWS::Region}
aws ec2 attach-volume --device /dev/sdh --volume-id ${dataVolumeId} --instance-id ${!INSTANCE_ID} --region ${AWS::Region}
while [ ! -e /dev/sdh ]; do
	echo "Waiting for device /dev/sdh";
	sleep 10s & wait ${!!};
done
file -sL /dev/sdh
if [ "$(file -sL /dev/sdh | cut -d' ' -f2)" == "data" ]; then
mkfs -t ext4 /dev/sdh
fi

# setup ECS
dnf install -y ecs-init
usermod -aG docker ec2-user

# mount docker volumes to data volume
mkdir -p /var/lib/docker/volumes
mount /dev/sdh /var/lib/docker/volumes

# ECS config
echo ECS_CLUSTER=${cluster} >> /etc/ecs/ecs.config
echo ECS_BACKEND_HOST= >> /etc/ecs/ecs.config
echo ECS_ENABLE_SPOT_INSTANCE_DRAINING=true >> /etc/ecs/ecs.config

# start ECS
systemctl enable --now --no-block ecs
