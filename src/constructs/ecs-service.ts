import { aws_ecs, aws_iam, aws_logs, CfnCondition, CfnDeletionPolicy, Fn, Stack } from "aws-cdk-lib";
import { CfnCluster, CfnTaskDefinition } from "aws-cdk-lib/aws-ecs";
import { Construct } from "constructs";

export interface EcsServiceProps {
	cluster: CfnCluster;
	taskMemory: string;
	containerDefinitions: CfnTaskDefinition.ContainerDefinitionProperty[];
	volumes?: CfnTaskDefinition.VolumeProperty[];
	runService?: 'true'|'false';
}

export class EcsService extends Construct {
	constructor(scope: Construct, id: string, props: EcsServiceProps) {
		super(scope, id);

		const {cluster, taskMemory, containerDefinitions, volumes, runService = 'true'} = props;

		/** CONDITIONS **/

		const runServiceCondition = new CfnCondition(this, 'RunServiceCondition', {
			expression: Fn.conditionEquals(runService, 'true')
		});

		/** RESOURCES **/

		const executionRole = this.createExecutionRole(props);

		const logGroup = new aws_logs.CfnLogGroup(this, 'LogGroup', {
			logGroupName: Stack.of(this).stackName
		});

		const taskDefinition = new aws_ecs.CfnTaskDefinition(this, 'TaskDefinition', {
			family: Stack.of(this).stackName,
			executionRoleArn: executionRole.roleArn,
			memory: taskMemory,
			requiresCompatibilities: [
				'EC2'
			],
			containerDefinitions: [
				...containerDefinitions.map(cd => ({
					logConfiguration: {
						logDriver: 'awslogs',
						options: {
							'awslogs-group': logGroup.ref,
							'awslogs-region': Stack.of(this).region,
							'awslogs-stream-prefix': 'ecs'
						}
					},
					...cd
				}))
			],
			volumes
		});

		taskDefinition.cfnOptions.updateReplacePolicy = CfnDeletionPolicy.RETAIN;

		const service = new aws_ecs.CfnService(this, 'Service', {
			cluster: cluster.ref,
			launchType: aws_ecs.LaunchType.EC2,
			serviceName: Stack.of(this).stackName,
			taskDefinition: taskDefinition.ref,
			desiredCount: 1,
			deploymentConfiguration: {
				minimumHealthyPercent: 0,
				maximumPercent: 100
			}
		});

		service.cfnOptions.condition = runServiceCondition;
	}

	protected createExecutionRole(props: EcsServiceProps) {
		const executionRole = new aws_iam.Role(this, 'ExecutionRole', {
			assumedBy: new aws_iam.ServicePrincipal('ecs-tasks.amazonaws.com')
		});

		executionRole.addManagedPolicy(aws_iam.ManagedPolicy.fromAwsManagedPolicyName('SecretsManagerReadWrite'));
		executionRole.addManagedPolicy(aws_iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AmazonECSTaskExecutionRolePolicy'));

		executionRole.addToPolicy(new aws_iam.PolicyStatement({
			effect: aws_iam.Effect.ALLOW,
			actions: [
				'kms:*',
				'ssm:GetParameters'
			],
			resources: ['*']
		}));

		return executionRole;
	}
}
