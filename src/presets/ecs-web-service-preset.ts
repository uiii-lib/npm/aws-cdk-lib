import { Stack } from 'aws-cdk-lib';
import { CfnTaskDefinition } from 'aws-cdk-lib/aws-ecs';

import { Preset } from '../core/preset';

import { EcsServicePreset, EcsServicePresetParameters } from './ecs-service-preset';

export interface EcsWebServicePresetParameters extends EcsServicePresetParameters {
	gitlabRegistryCredentials: string;
	certEmail: string;
}

export interface EcsWebServicePresetProps {
	taskMemory: string;
	containerDefinitions: CfnTaskDefinition.ContainerDefinitionProperty[];
	volumes?: CfnTaskDefinition.VolumeProperty[];
	openTcpPorts?: number[];
	parametersValues?: Partial<EcsWebServicePresetParameters>;
}

export class EcsWebServicePreset extends Preset<EcsWebServicePresetParameters> {
	constructor(stack: Stack, props: EcsWebServicePresetProps) {
		super(stack);

		const {taskMemory, containerDefinitions, volumes = [], openTcpPorts = [], parametersValues = {}} = props;

		const parameters = this.buildParameters(stack, {
			gitlabRegistryCredentials: {
				description: "Name of the secret (from Secrets Manager) with private registry credentials",
				type: "String"
			},
			certEmail: {
				description: "Email for the domain's SSL certificate",
				type: "String"
			},
		}, parametersValues);

		new EcsServicePreset(stack, {
			taskMemory,
			containerDefinitions: [
				{
					name: 'http-server',
					essential: true,
					image: 'registry.gitlab.com/uiii-lib/docker/nginx-proxy',
					repositoryCredentials: {
						credentialsParameter: parameters.gitlabRegistryCredentials.value
					},
					portMappings: [
						{
							hostPort: 80,
							containerPort: 80,
							protocol: 'tcp'
						},
						{
							hostPort: 443,
							containerPort: 443,
							protocol: 'tcp'
						}
					],
					mountPoints: [
						{
							sourceVolume: 'certs',
							containerPath: '/etc/nginx/certs'
						},
						{
							sourceVolume: 'vhost',
							containerPath: '/etc/nginx/vhost.d'
						},
						{
							sourceVolume: 'html',
							containerPath: '/usr/share/nginx/html'
						},
						{
							sourceVolume: 'docker-sock',
							containerPath: '/tmp/docker.sock',
							readOnly: true
						}
					],
					dockerLabels: {
						'com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy': ''
					}
				},
				{
					name: 'certbot',
					essential: true,
					image: 'nginxproxy/acme-companion',
					links: [
						'http-server'
					],
					mountPoints: [
						{
							sourceVolume: 'certs',
							containerPath: '/etc/nginx/certs'
						},
						{
							sourceVolume: 'vhost',
							containerPath: '/etc/nginx/vhost.d'
						},
						{
							sourceVolume: 'html',
							containerPath: '/usr/share/nginx/html'
						},
						{
							sourceVolume: 'acme',
							containerPath: '/etc/acme.sh'
						},
						{
							sourceVolume: 'docker-sock',
							containerPath: '/var/run/docker.sock',
							readOnly: true
						}
					],
					environment: [
						{
							name: 'DEFAULT_EMAIL',
							value: parameters.values.certEmail
						},
						{
							name: 'DEBUG',
							value: '1'
						}
					]
				},
				...containerDefinitions
			],
			volumes: [
				{
					name: 'certs',
					dockerVolumeConfiguration: {
						scope: 'shared',
						autoprovision: true
					}
				},
				{
					name: 'vhost',
					dockerVolumeConfiguration: {
						scope: 'shared',
						autoprovision: true
					}
				},
				{
					name: 'html',
					dockerVolumeConfiguration: {
						scope: 'shared',
						autoprovision: true
					}
				},
				{
					name: 'acme',
					dockerVolumeConfiguration: {
						scope: 'shared',
						autoprovision: true
					}
				},
				{
					name: 'docker-sock',
					host: {
						sourcePath: '/var/run/docker.sock'
					}
				},
				...volumes
			],
			openTcpPorts: [80, 443, 22, ...openTcpPorts],
			parameterValues: parameters.values
		});

		this.addParameterGroup(stack, "Domain", [
			'certEmail'
		]);
	}
}
