import { aws_ec2, aws_ecs, Fn, Stack } from 'aws-cdk-lib';
import { CfnTaskDefinition } from 'aws-cdk-lib/aws-ecs';

import { Preset } from '../core/preset';

import { EcsInstance } from '../constructs/ecs-instance';
import { EcsService } from '../constructs/ecs-service';

export interface EcsServicePresetParameters {
	instanceType: string;
	instanceImageId: string,
	swapSize: number;
	dataVolume: string;
	useSpotInstance: "true" | "false";
	vpcId: string;
	subnetIds: string[];
	ipAllocationId: string;
	sshAuthorizedKeysId: string;
	runApp: "true" | "false"
}

export interface EcsServicePresetProps {
	taskMemory: string;
	containerDefinitions: CfnTaskDefinition.ContainerDefinitionProperty[];
	volumes?: CfnTaskDefinition.VolumeProperty[];
	openTcpPorts?: number[];
	parameterValues?: Partial<EcsServicePresetParameters>
}

export class EcsServicePreset extends Preset<EcsServicePresetParameters> {
	constructor(stack: Stack, props: EcsServicePresetProps) {
		super(stack);

		const {taskMemory, containerDefinitions, volumes, openTcpPorts = [], parameterValues = {}} = props;

		const parameters = this.buildParameters(stack, {
			instanceType: {
				description: "Type of EC2 instance",
				type: "String"
			},
			instanceImageId: {
				description: "Instance image AMI ID",
				type: "String"
			},
			swapSize: {
				description: "Size of swap file in MB",
				type: "Number",
				default: "1024"
			},
			dataVolume: {
				description: "EBS volume ID for data",
				type: "String"
			},
			useSpotInstance: {
				description: "Use spot instances?",
				type: "String",
				allowedValues: [
					"true",
					"false"
				],
				default: "false"
			},
			vpcId: {
				description: "VPC",
				type: "AWS::EC2::VPC::Id"
			},
			subnetIds: {
				description: "Subnets",
				type: "List<AWS::EC2::Subnet::Id>"
			},
			ipAllocationId: {
				description: "Elastic IP allocation ID",
				type: "String"
			},
			sshAuthorizedKeysId: {
				description: "ID to be used to fetch authorized keys from https://uiii.gitlab.io/vps-authorized-keys",
				type: "String"
			},
			runApp: {
				description: "If false, the app service won't run",
				type: "String",
				allowedValues: [
					"true",
					"false"
				],
				default: "false"
			}
		}, parameterValues);

		this.addParameterGroup(stack, "Application", () => [
			"runApp"
		]);

		this.addParameterGroup(stack, "Networking", () => [
			"vpcId",
			"subnetIds",
			"ipAllocationId"
		]);

		this.addParameterGroup(stack, "EC2 Instance", () => [
			"instanceType",
			"instanceImageId",
			"useSpotInstance",
			"swapSize",
			"dataVolume",
			"sshAuthorizedKeysId",
		]);

		/** RESOURCES **/

		const cluster = new aws_ecs.CfnCluster(stack, 'Cluster', {
			clusterName: stack.stackName,
		});

		const securityGroup = new aws_ec2.CfnSecurityGroup(stack, 'SecurityGroup', {
			vpcId: parameters.vpcId.value,
			groupDescription: 'SG',
			securityGroupIngress: [
				...openTcpPorts.map(port => ({
					ipProtocol: 'tcp',
					fromPort: port,
					toPort: port,
					cidrIp: '0.0.0.0/0'
				})),
				{
					ipProtocol: 'icmp',
					fromPort: -1,
					toPort: -1,
					cidrIp: '0.0.0.0/0'
				}
			]
		});

		new EcsInstance(stack, 'Instance', {
			cluster,
			instanceType: parameters.instanceType.value,
			instanceImageId: parameters.instanceImageId.value,
			swapSize: parameters.swapSize.value,
			ipAllocationId: parameters.ipAllocationId.value,
			dataVolumeId: parameters.dataVolume.value,
			subnetId: Fn.select(0, parameters.subnetIds.value),
			securityGroupId: securityGroup.attrGroupId,
			sshAuthorizedKeysId: parameters.sshAuthorizedKeysId.value,
			createInstance: parameters.runApp.value,
			useSpotInstance: parameters.useSpotInstance.value
		});

		new EcsService(stack, 'Service', {
			cluster,
			taskMemory,
			containerDefinitions,
			volumes,
			runService: parameters.runApp.value
		});
	}
}
